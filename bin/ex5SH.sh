for i in {1..6};
do
./ex5 -snes_fd -snes_max_funcs 1e6 -snes_grid_sequence $i \
-da_refine 1 -ksp_rtol 1e-9 \
-pc_type lu -mms 1 \
-snes_converged_reason
done
