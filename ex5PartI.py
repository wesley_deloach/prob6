#! /usr/bin/env python
import os
import numpy as np
import matplotlib.pyplot as plt

N = [169, 625, 2401, 9409, 37249, 448225]
NMMS2 = [169, 625, 2401, 9409, 37249]
l2MMS1 = [2.20735e-6, 3.081e-7, 4.0427e-8, 5.16863e-9, 6.53061e-10, 8.20737e-11]
linfMMS1 = [7.955e-5, 2.42794e-5, 8.29001e-6, 2.62871e-6, 7.96102e-7, 2.3381e-7]
l2MMS2 = [0.00030817, 3.88389e-5, 4.97907e-6, 6.33007e-7, 7.97434e-8]
linfMMS2 = [0.0169964, 0.0039253, 0.0010981, 0.000367842, 0.000115646]

from pylab import legend, plot, loglog, show, title, xlabel, ylabel

loglog(N, l2MMS1, label = 'MMS1 l2 error')
loglog(N, linfMMS1, label = 'MMS1 linf error')
loglog(NMMS2, l2MMS2, label = 'MMS2 l2 error')
loglog(NMMS2, linfMMS2, label = 'MMS2 linf error')

title('l2 and linf error of MMS1 and MMS2')
ylabel('error')
xlabel('Degrees of freedom')
legend(loc='upper right')
show()
