#! /usr/bin/env python
import os
import numpy as np
import matplotlib.pyplot as plt

flops = []
flops2 = []
flopsGAMG = []
flops2GAMG = []

for k in range(1,5):
 modname = 'logLU%d' %k
 options1 = ['-snes_fd -snes_max_funcs 100000 -snes_grid_sequence %d'%k, '-snes_linesearch_type basic', 'da_refine 1 -ksp_rtol 1e-9', '-pc_type lu -mms 1', '-snes_converged_reason', '-log_view', ':%s.py:ascii_info_detail'%modname]
 os.system('./bin/ex5 '+' '.join(options1))
 perfmod = __import__(modname)
#For extracting flops
 flops.append(perfmod.LocalFlops[0])

 modname = 'logLU2%d' %k
 options2 = ['-snes_fd -snes_max_funcs 100000 -snes_grid_sequence %d'%k, '-snes_linesearch_type basic', 'da_refine 1 -ksp_rtol 1e-9', '-pc_type lu -mms 2', '-snes_converged_reason', '-log_view', ':%s.py:ascii_info_detail'%modname ]
 os.system('./bin/ex5 '+' '.join(options2))
 perfmod = __import__(modname)

#For extracting flops
 flops2.append(perfmod.LocalFlops[0])

 modname = 'loggamg%d' % k
 options3 = ['-snes_fd -snes_max_funcs 100000 -snes_grid_sequence %d'%k, '-da_refine 1', '-ksp_rtol 1e-9', '-pc_type gamg -mms 1', '-snes_converged_reason -ksp_converged_reason', '-log_view', ':%s.py:ascii_info_detail'%modname ]
 os.system('./bin/ex5 '+' '.join(options3))
 perfmod = __import__(modname)

#For extracting flops
 flopsGAMG.append(perfmod.LocalFlops[0])

 modname = 'loggamg2%d' % k
 options4 = ['-snes_fd -snes_max_funcs 100000 -snes_grid_sequence %d'%k, '-da_refine 1', '-ksp_rtol 1e-9', '-pc_type gamg -mms 2', '-snes_converged_reason -ksp_converged_reason', '-log_view', ':%s.py:ascii_info_detail'%modname ]
 os.system('./bin/ex5 '+' '.join(options4))
 perfmod = __import__(modname)

#For extracting flops
 flops2GAMG.append(perfmod.LocalFlops[0])

l2error = [2.20735e-6, 3.081e-7, 4.0427e-8, 5.16863e-9]
l2errorMMS2 = [0.00030817, 3.88389e-5, 4.97907e-6, 6.33007e-7]
l2errorGAMG = [2.20735e-6, 3.081e-7, 4.0427e-8, 5.16863e-9]
l2errorGAMG2 = [0.00030817, 3.88389e-5, 4.97907e-6, 6.33007e-7]
from pylab import legend, plot, loglog, show, title, xlabel, ylabel

loglog(flops, l2error, label='Newton lu MMS1')
loglog(flops2, l2errorMMS2, label='Newton lu MMS2')
loglog(flopsGAMG, l2errorGAMG, label='GAMG MMS1')
loglog(flops2GAMG, l2errorGAMG2, label='GAMG MMS2')
title('LU vs GAMG')
ylabel('error')
xlabel('flops')
legend(loc='upper left')
show()
